# Docker Compose, Apache, PHP, and MariaDB

This is a variant of the [Docker Compose, Flask, and
MongoDB](https://gitlab.com/TrendDotFarm/docker-compose-flask-mongo) project.

## GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file can be used in GitLab to build, test,
and deploy the code. For more information, read the [Docker Compose Integration
to GitLab CI](GitLab-CI.md) guide.
